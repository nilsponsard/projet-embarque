#include "Nokia_5110.h"
#include <Servo.h>

#define BP PC13

#define RST 7
#define CE 6
#define DC 5
#define DIN 4
#define CLK 3

#define broche_servoA 10 // broche servo A
#define broche_servoB 11 // broche servo B

#define LED_PIN 12
#define CPT_CG A4
#define CPT_EG A5
#define CPT_CD A3
#define CPT_ED A2

#define TRIG 9
#define ECHO 8

int cg;
int ed;
int cd;
int eg;

int lastMilli;
int leftOffset;
int rightOffset;
int forwardSpeed;

const double K = 3.43 / 100.0 / 2.0;
const int threshold = 750;
const int minDistance = 20; // cm

Nokia_5110 lcd = Nokia_5110(RST, CE, DC, DIN, CLK);

Servo servoA; // crée un objet servo pour contrôler le servomoteur A
Servo servoB; // crée un objet servo pour contrôler le servomoteur B

constexpr int MAX_SENS2 =
    900; // largeur impulsion pour position ANGLE_MIN degrés du servomoteur
constexpr int ARRET =
    1500; // largeur impulsion pour position ANGLE_MEDIANE degrés du servomoteur
constexpr int MAX_SENS1 =
    2100; // largeur impulsion pour position ANGLE_MAX degrés du servomoteur

constexpr unsigned LOOPDELAY = 15;
constexpr unsigned DISPLAY_DELAY = 500;
struct State
{
    bool on;
    bool avoiding;
    unsigned isEndCpt;
    bool left;

    const unsigned MAX_IS_END_CPT = 5;

    State(bool p_on, bool p_avoiding) : on(p_on), avoiding(p_avoiding), isEndCpt(0), left(false)
    {
    }
};

State state(false, false);

void toggle()
{
    state.on = !state.on;
}

void setup()
{
    // bouton
    pinMode(BP, INPUT_PULLUP);
    attachInterrupt(BP, toggle, FALLING);

    leftOffset = 0;
    rightOffset = 0;
    forwardSpeed = 600;

    pinMode(TRIG, OUTPUT);
    pinMode(ECHO, INPUT);
    digitalWrite(TRIG, LOW);

    pinMode(LED_PIN, OUTPUT);
    digitalWrite(LED_PIN, LOW);
}

// le centre gauche est sur la ligne et le centre droit est à coté
void loop()
{
    // affichage toutes les secondes
    if (lastMilli + DISPLAY_DELAY < millis())
    {
        lastMilli = millis();
        display();
    }
    getCpt();
    if (state.on)
    {
        if (isEnd())
        {
            ++state.isEndCpt;
            if (state.isEndCpt == state.MAX_IS_END_CPT)
            {
                state.on = false;
                state.isEndCpt = 0;
            }
        }
        else
        {
            if (isAligned())
            {
                leftOffset = 0;
                rightOffset = 0;
            }
            else
            {
                if (eg > threshold)
                {
                    rightOffset = -forwardSpeed + 25;
                }
                else if (cg < threshold)
                {
                    if (leftOffset > -450)
                        leftOffset -= 10;
                }
                else if (cd > threshold)
                {
                    if (rightOffset > -450)
                        rightOffset -= 10;
                }
                if (ed > threshold)
                {
                    leftOffset = -forwardSpeed + 25;
                }
            }
            applyMotorOffset();
        }
    }
    else
    {
        motorStop();
    }
    delay(LOOPDELAY);
}

bool isEnd()
{
    return eg > threshold && ed > threshold;
}

bool isAligned()
{
    // si le robot est correctement aligné, seul le centre gauche est sur la ligne
    return ed < threshold && eg < threshold && cd < threshold && cg > threshold;
}

void display()
{
    lcd.clear();
    lcd.setCursor(5 * 5, 0);
    lcd.print(cd > threshold ? 'X' : 'O');
    lcd.setCursor(10 * 5, 0);
    lcd.print(cg > threshold ? 'X' : 'O');

    lcd.setCursor(0, 1);
    lcd.print(ed > threshold ? "X" : "O");
    lcd.setCursor(15 * 5, 1);
    lcd.print(eg > threshold ? "X" : "O");

    lcd.setCursor(0, 2);
    long d = getDistanceUS();
    lcd.print(d < minDistance ? "Obstacle" : "Pas obstacle");
    lcd.setCursor(0, 3);
    lcd.print(d);
    lcd.print("cm");
    lcd.setCursor(0, 5);
    lcd.print(state.on);
    lcd.print(" | ");
    lcd.print(state.avoiding);
}

void getCpt()
{
    digitalWrite(LED_PIN, HIGH);
    cg = analogRead(CPT_CG);
    eg = analogRead(CPT_EG);
    cd = analogRead(CPT_CD);
    ed = analogRead(CPT_ED);
    digitalWrite(LED_PIN, LOW);
}

long getDistanceUS()
{
    digitalWrite(TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite(TRIG, LOW);
    long d = pulseIn(ECHO, HIGH);
    return d * K;
}

void attach()
{
    if (!servoA.attached())
        servoA.attach(broche_servoA);
    if (!servoB.attached())
        servoB.attach(broche_servoB);
}

// void motorGoSlighlyRight() {
//   attach();
//   servoA.writeMicroseconds(ARRET - forwardSpeed -
//                            leftOffset);           // crée impulsion - sens2
//   servoB.writeMicroseconds(ARRET + forwardSpeed); // crée impulsion - sens2
// }

void applyMotorOffset()
{
    attach();
    servoA.writeMicroseconds(ARRET - forwardSpeed -
                             rightOffset); // crée impulsion - sens2

    servoB.writeMicroseconds(ARRET + forwardSpeed +
                             leftOffset); // crée impulsion - sens2
}

void motorGoForward()
{
    attach();
    servoA.writeMicroseconds(MAX_SENS2); // crée impulsion - sens2
    servoB.writeMicroseconds(MAX_SENS1); // crée impulsion - sens1
}

void motorGoBackward()
{
    attach();
    servoA.writeMicroseconds(ARRET + forwardSpeed); // crée impulsion - sens1
    servoB.writeMicroseconds(ARRET - forwardSpeed); // crée impulsion - sens2
}

void motorGoRight()
{
    attach();
    servoA.writeMicroseconds(ARRET - forwardSpeed); // crée impulsion - sens2

    servoB.writeMicroseconds(ARRET); // crée impulsion - sens2
}

void motorGoLeft()
{
    attach();
    servoA.writeMicroseconds(ARRET); // crée impulsion - sens1

    servoB.writeMicroseconds(ARRET + forwardSpeed); // crée impulsion - sens1
}

void motorStop()
{
    if (servoA.attached())
        servoA.detach(); // détache le servomoteur de la broche  = arret propre
                         // servomoteur
    if (servoB.attached())
        servoB.detach(); // détache le servomoteur de la broche  = arret propre
                         // servomoteur
}
